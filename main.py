import random
from threading import Thread

import mcstatus
import requests

servers = []


def get_ip_country(ip):
    return requests.get(f'https://ipapi.co/{ip}/json/').json().get("country_name")


def get_random_ip():
    return (str(random.randint(0, 255)) + "." + str(random.randint(0, 255)) + "." +
            str(random.randint(0, 255)) + "." + str(random.randint(0, 255)))


def ping(ip):
    server = mcstatus.JavaServer.lookup(ip + ":25565")
    try:
        status = server.status()
    except:
        return
    if status.players.online > 0:
        print("----------------------------------")
        print(ip)
        print("Players: " + str(status.players.online) + " / " + str(status.players.max))
        print("Version: " + str(status.version.name))
        print("Country: " + get_ip_country(ip))
        print("----------------------------------\n")
    else:
        print(ip + " - 0 - " + get_ip_country(ip))
    servers.append(ip)


if __name__ == "__main__":
    for i in range(0, 255 ** 4):
        Thread(target=ping, args=[get_random_ip()]).start()
    for i in servers:
        print(i)
